import arrow
from eyewitness.image_utils import PostBytesImageProducer
from eyewitness.image_id import ImageId


if __name__ == '__main__':
    image_producer = PostBytesImageProducer(host='localhost:5566')
    with open('eyewitness/test/pics/pikachu.png', 'rb') as f:
        image_bytes = f.read()
    image_id = ImageId(channel='pikachu', timestamp=arrow.now().timestamp, file_format='png')
    image_producer.produce_image(
        image_id=image_id, image_bytes=image_bytes, raw_image_path='pika_bytes_python.png')
