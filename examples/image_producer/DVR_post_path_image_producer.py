import arrow
import requests
import shutil
import time
from pathlib import Path

from eyewitness.image_id import ImageId
from eyewitness.image_utils import PostFilePathImageProducer


class DVRImageGenerator(object):
    def __init__(self, raw_image_dir, timesleep=8, ip='localhost', port='5566'):
        self.raw_image_dir = raw_image_dir
        self.timesleep = timesleep
        self.sess = requests.Session()
        self.image_poster = PostFilePathImageProducer(host='%s:%s' % (ip, port))

    def fetch_and_post_DVR_image(self, raw_image_path, ch, image_id):
        try:
            # TODO: this is a fake DVR example for fetching image from different channel
            resp = self.sess.get('http://192.168.1.110/cgi-bin/web_jpg.cgi?ch=' + str(ch),
                                 auth=('admin', '123456'), stream=True)

            if resp.status_code == 200:
                with open(raw_image_path, 'wb') as f:
                    resp.raw.decode_content = True
                    shutil.copyfileobj(resp.raw, f)
                print("senting %s" % str(image_id))
                self.image_poster.produce_image(
                    image_id=image_id,
                    raw_image_path=raw_image_path)
            else:
                print("connection err due to ", resp.status_code)
        except Exception as e:
            print(e)

    def generate_images(self):
        while True:
            for channel in range(8):
                timestamp = arrow.now().timestamp
                image_id = ImageId(channel=str(channel), timestamp=timestamp, file_format='jpg')
                file_name = "%s--%s.%s" % (
                    image_id.channel, image_id.timestamp, image_id.file_format)
                raw_image_path = str(Path(self.raw_image_dir, file_name))
                self.fetch_and_post_DVR_image(raw_image_path, channel, image_id)
            time.sleep(self.timesleep)


if __name__ == '__main__':
    image_generator = DVRImageGenerator('/opt/caffe/raw_image')
    image_generator.generate_images()
