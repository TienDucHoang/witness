from eyewitness.config import (
    BBOX,
    BoundedBoxObject,
    DRAWN_IMAGE_PATH,
    IMAGE_ID,
    DETECTED_OBJECTS,
)
from eyewitness.flask_server import BboxObjectDetectionFlaskWrapper
from eyewitness.image_utils import ImageHandler
from eyewitness.object_detector import ObjectDetector
from eyewitness.detection_utils import (DetectionResultHandler, DetectionResult)
from eyewitness.result_handler.db_writer import BboxPeeweeDbWriter
from peewee import SqliteDatabase


class FakeObjDetector(ObjectDetector):
    def __init__(self, enable_draw_bbox=True):
        self.enable_draw_bbox = enable_draw_bbox

    def detect(self, image_obj):
        """
        fake detect method for FakeObjDetector

        Parameters
        ----------
        image_obj: eyewitness.image_util.Image

        Returns
        -------
        DetectionResult
        """
        image_dict = {
            IMAGE_ID: image_obj.image_id,
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(250, 100, 800, 900, 'pikachu', 0.5, ''))
            ],
            DRAWN_IMAGE_PATH: 'pikachu_test.png'
        }
        detection_result = DetectionResult(image_dict)
        if self.enable_draw_bbox:
            ImageHandler.draw_bbox(image_obj.pil_image_obj, detection_result.detected_objects)
            ImageHandler.save(image_obj.pil_image_obj, image_dict[DRAWN_IMAGE_PATH])

        return detection_result


class FakeDetectionResultHandler(DetectionResultHandler):
    @property
    def detection_method(self):
        return BBOX

    def _handle(self, detection_result):
        print("I'm just a fake detection result handler")
        pass


if __name__ == '__main__':
    sqlite_db_path = 'test.sqlite'
    database = SqliteDatabase(sqlite_db_path)
    object_detector = FakeObjDetector()
    bbox_sqlite_handler = BboxPeeweeDbWriter(database)
    fake_result_handler = FakeDetectionResultHandler()
    result_handlers = [bbox_sqlite_handler, fake_result_handler]

    # TODO isolate the image Register from bbox_sqlite_handler
    flask_wrapper = BboxObjectDetectionFlaskWrapper(
        object_detector, bbox_sqlite_handler, result_handlers, database=database,
        with_admin=True)
    params = {'host': 'localhost', 'port': 5566, 'threaded': False}
    flask_wrapper.app.run(**params)
