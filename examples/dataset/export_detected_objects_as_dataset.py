from eyewitness.dataset_util import create_bbox_dataset_from_eyewitness
from eyewitness.dataset_util import BboxDataSet

from peewee import SqliteDatabase

if __name__ == '__main__':
    # your own db obj
    db_file = '/keras-yolo3/db_folder/example.sqlite'
    database = SqliteDatabase(db_file)

    # select classes that want to be export
    valid_classes = set(['person'])
    output_dataset_folder = '/keras-yolo3/db_folder/MAS'
    dataset_name = 'MAS'

    # create dataset files
    create_bbox_dataset_from_eyewitness(
        database, valid_classes, output_dataset_folder, dataset_name)

    dataset_A = BboxDataSet(output_dataset_folder, dataset_name)
    dataset_A.generate_train_test_list(train_ratio=0.5)
