from eyewitness.dataset_util import BboxDataSet

if __name__ == "__main__":
    dataset_folder = '/home/stream/VOC_dataset/VOCdevkit/VOC2007'
    dataset_VOC2007 = BboxDataSet(dataset_folder, 'VOC2007')

    dataset_folder = '/home/stream/VOC_dataset/MAS_dataset'
    dataset_MAS = BboxDataSet(dataset_folder, 'MAS')
    valid_labels = dataset_MAS.valid_labels

    output_dataset_folder = '/home/stream/VOC_dataset/VOCxMAS'
    dataset_MAS = BboxDataSet.union_bbox_datasets(
        [dataset_VOC2007, dataset_MAS], output_dataset_folder, 'VOCxMAS',
        filter_labels=valid_labels, remove_empty_labels_file=True)
    dataset_MAS.convert_into_darknet_format()
