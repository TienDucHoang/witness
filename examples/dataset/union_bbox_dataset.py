from eyewitness.dataset_util import BboxDataSet

if __name__ == "__main__":
    dataset_folder = '/home/stream/Documents/VOCdevkit/VOCdevkit/VOC2007'
    dataset_A = BboxDataSet(dataset_folder, 'VOC2007_1')
    print(dataset_A.valid_labels)

    dataset_B = BboxDataSet(dataset_folder, 'VOC2007_2')
    dataset_C = BboxDataSet.union_bbox_datasets(
        [dataset_A, dataset_B], 'VOC2007_fake', 'VOC2007_fake')
    dataset_C.generate_train_test_list()
