## a fake flask object detection example:
```
# start a server at http://localhost:5566/
# and a ADMIN made from flask-admin at http://localhost:5566/admin/
# you can implement your own ObjectDetector, DetectionResultHandler
python examples/run_flask_server.py

# post pikachu image bytes to flask server
# which will stores raw pikachu.png and drawn pikachu_test.png at workspace


# post by channel
curl -X POST \
  http://localhost:5566/detect_post_bytes \
  -H 'content-type: application/json' \
  -H 'image_id: pikachu--1541860146--png'  \
  -H 'raw_image_path: ./pikachu_raw.png' \
  --data-binary "@eyewitness/test/pics/pikachu.png"


# post path of existing pikachu image file to flask server
# which will stores drawn pikachu.png at workspace
curl -X POST \
  http://localhost:5566/detect_post_path \
  -H 'content-type: application/json' \
  -H 'image_id: pikachu--1541860141--png' \
  -H 'raw_image_path: ./eyewitness/test/pics/pikachu.png'

# a python image producer (post_bytes) example
python examples/post_bytes_image_producer.py

# a python image producer (post_path) example
python examples/post_path_image_producer.py
```

## Detection result handler examples:

### Line msg sender
```bash
# you can register your own Line Messaging API channel at https://developers.line.me
# make sure you have create line channel, and subscribe your own channel
export CHANNEL_ACCESS_TOKEN=<your-own-channel-tokenizer>
export YOUR_USER_ID=<your-line-user-id>
python examples/detection_result_handler/line_detection_result_handler_example.py

# you should get a pikachu button annotation
```

### Facebook msg sender

this example will sent a pikachu image to the facebook-user-id you gave, and will listen to msgs
once there are a msg with '5566' chat with your account, the robot will reply the sentence like 
```
What makes me sad, is that I give up you and love, and my dream is also broken, but I need to fight back tears
 ```

```bash
# you need to generate the fb session file first by
# from fbchat import Client
# client = Client("your-fb-email", "your-fb-password")  # also might need to auth your connection
# with open('session.json', 'w') as cf:  # thus dump your session setting into file
#    json.dump(cookies, cf)
# client.uid # this is your own fbid

export FACEBOOK_USER_EMAIL=<your-facebook-email>
export FACEBOOK_USER_PASSWORD=<your-facebook-password>
export FACEBOOK_SESSION_COOKIES_PATH=<your-facebook-session-file-path>  # the session.json above
export YOUR_USER_ID=<your-facebook-user-id>
python examples/detection_result_handler/facebook_detection_result_handler_example.py
```
