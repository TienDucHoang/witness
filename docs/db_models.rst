ORM DB Models
=============
orm models for Eyewitness with the support of peewee


ImageInfo
---------

.. code-block:: python

   class ImageInfo(BaseModel):
      image_id = CharField(unique=True, primary_key=True)
      channel = CharField()
      file_format = CharField()
      timestamp = TimestampField()
      raw_image_path = CharField(null=True)
      drawn_image_path = CharField(null=True)

BboxDetectionResult
-------------------

.. code-block:: python

   class BboxDetectionResult(BaseModel):
      image_id = ForeignKeyField(ImageInfo)
      x1 = IntegerField()
      x2 = IntegerField()
      y1 = IntegerField()
      y2 = IntegerField()
      label = CharField()
      meta = CharField()
      score = DoubleField()

RegisteredAudience
------------------

.. code-block:: python

   class RegisteredAudience(BaseModel):
      audience_id = CharField(unique=True, primary_key=True)
      user_id = CharField(null=False)
      platform_id = CharField(null=False)
      register_time = TimestampField()
      description = CharField()


FalseAlertFeedback
------------------

.. code-block:: python

   class FalseAlertFeedback(BaseModel):
      # peewee didn't support compositeKey as foreignKey, using field to specify field
      audience_id = ForeignKeyField(RegisteredAudience)
      image_id = ForeignKeyField(ImageInfo, null=True)
      receive_time = TimestampField()
      feedback_meta = CharField()
      # TODO: if the is_false_alert field needed??
      is_false_alert = BooleanField()


BboxAnnotationFeedback
----------------------

.. code-block:: python

   class BboxAnnotationFeedback(BaseModel):
      # peewee didn't support compositeKey as foreignKey, using field to specify field
      audience_id = ForeignKeyField(RegisteredAudience)
      image_id = ForeignKeyField(ImageInfo, null=True)
      receive_time = TimestampField()
      feedback_meta = CharField()
      is_false_alert = BooleanField()
      x1 = IntegerField()
      x2 = IntegerField()
      y1 = IntegerField()
      y2 = IntegerField()
      label = CharField()
