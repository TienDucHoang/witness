Object Detector
===============
a module define the object detector interface

.. automodule:: eyewitness.object_detector
    :members:
    :undoc-members:
    :show-inheritance:

