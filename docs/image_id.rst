ImageId
=======
a module used to represent a image, and store image information

.. automodule:: eyewitness.image_id
    :members:
    :undoc-members:
    :show-inheritance:
