Image Utils
===========
Util methods for operation on image


.. automodule:: eyewitness.image_utils
    :members:
    :undoc-members:
    :show-inheritance:
