Feedback Msg Utils
==================
Utils modules used for Feedback Msg


.. automodule:: eyewitness.feedback_msg_utils
    :members:
    :undoc-members:
    :show-inheritance:
