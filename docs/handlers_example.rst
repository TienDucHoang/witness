Handler Example
===============
Handlers for detection results

DB Writer
---------

.. automodule:: eyewitness.result_handler.db_writer
    :members:
    :undoc-members:
    :show-inheritance:


Line msg Sender
---------------

.. automodule:: eyewitness.result_handler.line_detection_result_handler
    :members:
    :undoc-members:
    :show-inheritance:
