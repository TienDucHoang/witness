Detection Utils
===============
Utils modules used for detection


.. automodule:: eyewitness.detection_utils
    :members:
    :undoc-members:
    :show-inheritance:
