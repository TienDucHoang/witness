Evaluation Utils
================
used to calculate the detector performance
currently support mAP for object detection

.. automodule:: eyewitness.evaluation
    :members:
    :undoc-members:
    :show-inheritance:


A BboxMAPEvaluator Example
==========================

.. code-block:: python

   # a evaluation example with yolov3 detector
   # https://github.com/penolove/keras-yolo3/blob/eyeWitnessWrapper/eyewitness_evaluation.py
   from eyewitness.config import DATASET_TEST_ONLY
   dataset_folder = 'VOC2007'
   dataset_VOC_2007 = BboxDataSet(dataset_folder, 'VOC2007')
   object_detector = YoloV3DetectorWrapper(args, threshold=0.0)
   bbox_map_evaluator = BboxMAPEvaluator(dataset_mode=DATASET_TEST_ONLY)
   # which will lead to ~0.73
   print(bbox_map_evaluator.evaluate(object_detector, dataset_VOC_2007)['mAP'])
