Audience Id
===========
a module used to represent a audience, and store audience information

.. automodule:: eyewitness.audience_id
    :members:
    :undoc-members:
    :show-inheritance:
