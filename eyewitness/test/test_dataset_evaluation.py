import unittest
from pathlib import Path

from eyewitness.config import (
    BBOX,
    IMAGE_ID,
    DETECTED_OBJECTS,
    BoundedBoxObject,
    DETECTION_METHOD,
    DATASET_TEST_ONLY,
    DATASET_ALL,
)
from eyewitness.dataset_util import BboxDataSet
from eyewitness.detection_utils import DetectionResult
from eyewitness.evaluation import BboxMAPEvaluator
from eyewitness.image_utils import ImageHandler
from eyewitness.object_detector import ObjectDetector


class FakePersonDetector(ObjectDetector):
    def __init__(self, enable_draw_bbox=True):
        self.enable_draw_bbox = enable_draw_bbox

    def detect(self, image_obj):
        """
        fake detect method for FakeObjDetector

        Parameters
        ----------
        image_obj: eyewitness.image_utils.Image

        Returns
        -------
        DetectionResult

        """
        image_dict = {
            IMAGE_ID: image_obj.image_id,
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(8, 12, 352, 498, 'person', 0.5, ''))
            ],
            DETECTION_METHOD: BBOX
        }
        if self.enable_draw_bbox:
            ImageHandler.draw_bbox(image_obj.pil_image_obj, image_dict[DETECTED_OBJECTS])

        detection_result = DetectionResult(image_dict)
        return detection_result

    @property
    def valid_labels(self):
        return {'person'}


class DataSetTest(unittest.TestCase):
    def test_convert2darknet_data(self):
        dataset_folder = str(Path('eyewitness', 'test', 'dataset', 'VOC2007_for_unitest'))
        dataset_A = BboxDataSet(dataset_folder, 'VOC2007_for_unitest')
        dataset_A.convert_into_darknet_format()

    def test_dataset_methods(self):
        dataset_folder = str(Path('eyewitness', 'test', 'dataset', 'VOC2007_for_unitest'))
        dataset_A = BboxDataSet(dataset_folder, 'VOC2007_for_unitest')
        self.assertEqual(dataset_A.valid_labels,
                         {'dog', 'person', 'car', 'chair', 'train', 'sofa'})
        self.assertEqual(dataset_A.dataset_type, BBOX)
        train_vali_ids = list(dataset_A.training_and_validation_set)
        self.assertListEqual(train_vali_ids, ['000001', '000002', '000005--00000--jpg'])

        testing_ids = list(dataset_A.testing_set)
        self.assertListEqual(testing_ids, ['000003', '000004'])

        selected_images = dataset_A.get_selected_images(mode=DATASET_ALL)
        all_image_objs = list(dataset_A.image_obj_iterator(selected_images))
        self.assertEqual(len(all_image_objs), 5)
        self.assertSetEqual(
            set([i.image_id.channel for i in all_image_objs]),
            set(['000003', '000004', '000001', '000002', '000005']))

        selected_images = dataset_A.get_selected_images(mode=DATASET_TEST_ONLY)
        testing_image_objs = list(dataset_A.image_obj_iterator(selected_images))
        self.assertEqual(len(testing_image_objs), 2)
        self.assertListEqual(
            [i.image_id.channel for i in testing_image_objs],
            ['000003', '000004'])

        selected_images = dataset_A.get_selected_images(mode=DATASET_ALL)
        img_gt_objs = list(dataset_A.ground_truth_iterator(selected_images))
        self.assertEqual(len(img_gt_objs), 5)
        self.assertTupleEqual(img_gt_objs[0][0], (48, 240, 195, 371, 'dog', 1, ''))

        dataset_images_with_gt = list(
            dataset_A.dataset_iterator(with_gt_objs=True, mode=DATASET_ALL))
        self.assertEqual(len(dataset_images_with_gt), 5)
        all_objs = [j for i in dataset_images_with_gt for j in i[1]]
        self.assertEqual(len(all_objs), 19)

    def test_evaluation(self):
        dataset_folder = str(Path('eyewitness', 'test', 'dataset', 'VOC2007_for_unitest'))
        dataset_A = BboxDataSet(dataset_folder, 'VOC2007_for_unitest')
        fake_person_detector = FakePersonDetector()
        bbox_map_evaluator = BboxMAPEvaluator(dataset_mode=DATASET_TEST_ONLY, logging_frequency=1)
        bbox_map_evaluator.evaluate(fake_person_detector, dataset_A)

    def test_generate_list(self):
        dataset_folder = str(
            Path('eyewitness', 'test', 'dataset', 'VOC2007_for_unitest_without_list'))
        dataset_A = BboxDataSet(dataset_folder, 'VOC2007_for_unitest_2')
        dataset_A.generate_train_test_list()
