import unittest

import arrow
from peewee import SqliteDatabase
from eyewitness.audience_id import AudienceId
from eyewitness.config import (
    FEEDBACK_NO_OBJ,
    IMAGE_ID,
    AUDIENCE_ID,
    FEEDBACK_METHOD,
    RECEIVE_TIME,
    FEEDBACK_META,
    BoundedBoxObject,
    DETECTED_OBJECTS,
    DRAWN_IMAGE_PATH,
    RAW_IMAGE_PATH,
)
from eyewitness.detection_utils import DetectionResult
from eyewitness.detection_result_filter import FeedbackBboxDeNoiseFilter
from eyewitness.feedback_msg_utils import FeedbackMsg
from eyewitness.image_id import ImageId
from eyewitness.result_handler.db_writer import (
    FalseAlertPeeweeDbWriter,
    BboxPeeweeDbWriter,
)


class FeedbackDenoiseTest(unittest.TestCase):
    def test_false_alter_feedback_denoise_filter(self):
        # using memory db for testing
        db_obj = SqliteDatabase(':memory:')
        false_alert_feedback_handler = FalseAlertPeeweeDbWriter(db_obj)
        bbox_sqlite_handler = BboxPeeweeDbWriter(db_obj)

        # prepare two detection result, on will be false alert
        image_id_false_alert = ImageId('channel1', 1541860141)
        image_id_normal = ImageId('channel2', 1541860141)
        for image_id in (image_id_false_alert, image_id_normal):
            image_dict = {
                IMAGE_ID: image_id,
                DETECTED_OBJECTS: [
                    BoundedBoxObject(*(1, 1, 2, 2, 'person', 0.5, '')),
                    BoundedBoxObject(*(2, 2, 4, 4, 'person', 0.5, '')),
                ],
                DRAWN_IMAGE_PATH: 'drawn_minhan.jpg',
            }
            image_meta = {RAW_IMAGE_PATH: 'minhan.jpg'}
            detection_result = DetectionResult(image_dict)
            bbox_sqlite_handler.register_image(detection_result.image_id, image_meta)
            bbox_sqlite_handler.handle(detection_result)

        # prepare a feedback information, insert registered_user, feedback msg
        audience_id = AudienceId(user_id='min-han', platform_id='line')
        false_alert_feedback_handler.register_audience(audience_id, {'description': 'handsome'})

        # insert the false alert msg
        arrive_time = arrow.now()
        feedback_dict = {
            AUDIENCE_ID: audience_id, RECEIVE_TIME: arrive_time.timestamp,
            FEEDBACK_METHOD: FEEDBACK_NO_OBJ, IMAGE_ID: image_id_false_alert,
            FEEDBACK_META: 'mic_test'
        }
        feedback_obj = FeedbackMsg(feedback_dict)
        false_alert_feedback_handler.handle(feedback_obj)

        # test what the feedback denoise get
        denoise_filter = FeedbackBboxDeNoiseFilter(db_obj, detection_threshold=0.0)
        false_alert_bbox = denoise_filter.false_alert_feedback_bbox
        self.assertEqual(len(false_alert_bbox), 1)  # should only have 1 channel x class there
        self.assertEqual(false_alert_bbox[('channel1', 'person')], [(1, 1, 2, 2), (2, 2, 4, 4)])

        # test if the detection result filter work properly:
        # channel with feedback info should only have 0.5 * 0.9 scores
        # channel without feedback infor should still 0.5 scores
        for image_id, expected_score in ((image_id_false_alert, 0.45), (image_id_normal, 0.5)):
            image_dict = {
                IMAGE_ID: image_id,
                DETECTED_OBJECTS: [
                    BoundedBoxObject(*(1, 1, 2, 2, 'person', 0.5, '')),
                    BoundedBoxObject(*(2, 2, 4, 4, 'person', 0.5, '')),
                ],
                DRAWN_IMAGE_PATH: 'drawn_minhan.jpg',
            }
            image_meta = {RAW_IMAGE_PATH: 'minhan.jpg'}
            detection_result = DetectionResult(image_dict)
            filtered_detection_result = denoise_filter.apply(detection_result)
            self.assertEqual(set([expected_score]),
                             set(obj.score for obj in filtered_detection_result.detected_objects))
