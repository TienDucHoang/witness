import unittest

from eyewitness.audience_id import AudienceId


class AudienceInfoTest(unittest.TestCase):
    def test_image_id_cmp(self):
        user1 = AudienceId('line', 'minhan_hgdfmjg2715')
        user2 = AudienceId('facebook', 'minhan_hgdfmjg2715')
        user3 = AudienceId('line', 'hgdfmjg2715')
        self.assertNotEqual(user1, user2)
        self.assertNotEqual(user1, user3)

    def test_image_id_as_key(self):
        user1 = AudienceId('line', 'minhan_hgdfmjg2715')
        user2 = AudienceId('facebook', 'minhan_hgdfmjg2715')
        x = {user1: 5566, user2: 1126}
        self.assertEqual(x[user1], 5566)
        self.assertEqual(x[user2], 1126)
