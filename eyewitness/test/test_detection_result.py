import json
import unittest

from eyewitness.detection_utils import DetectionResult
from eyewitness.image_id import ImageId
from eyewitness.config import (
    BBOX,
    BoundedBoxObject,
    DETECTED_OBJECTS,
    DETECTION_METHOD,
    DRAWN_IMAGE_PATH,
    IMAGE_ID,
)


class DetectionResultTest(unittest.TestCase):
    def test_detection_result_init(self):
        image_dict = {
            IMAGE_ID: ImageId('channel1', 1541860141),
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(1, 1, 2, 2, 'person', 0.5, '')),
                BoundedBoxObject(*(2, 2, 4, 4, 'person', 0.5, '')),
            ]
        }
        detection_result = DetectionResult(image_dict)
        self.assertEqual(str(detection_result.image_id), 'channel1--1541860141--jpg')
        self.assertEqual(len(detection_result.detected_objects), 2)
        self.assertEqual(detection_result.drawn_image_path, '')

    def test_detection_result_to_dict(self):
        image_dict = {
            IMAGE_ID: ImageId('channel1', 1541860141, 'jpg'),
            DETECTED_OBJECTS: [
                BoundedBoxObject(*(1, 1, 2, 2, 'person', 0.5, '')),
                BoundedBoxObject(*(2, 2, 4, 4, 'person', 0.5, '')),
            ]
        }
        detection_result = DetectionResult(image_dict)
        image_dict_2 = detection_result.to_json_dict()
        self.assertEqual(str(image_dict_2[IMAGE_ID]), 'channel1--1541860141--jpg')
        self.assertEqual(len(image_dict_2[DETECTED_OBJECTS]), 2)
        self.assertEqual(image_dict_2.get(DRAWN_IMAGE_PATH, ''), '')

    def test_detection_result_from_json(self):
        detection_result_dict = {
            IMAGE_ID: str(ImageId('channel1', 1541860141)),
            DETECTION_METHOD: BBOX,
            DETECTED_OBJECTS: [
                (1, 1, 2, 2, 'person', 0.5, ''),
                (2, 2, 4, 4, 'person', 0.5, ''),
            ]
        }
        detection_result_json_dict = json.dumps(detection_result_dict)
        detection_result = DetectionResult.from_json(detection_result_json_dict)
        self.assertTrue(
            all(isinstance(i, BoundedBoxObject) for i in detection_result.detected_objects))
        self.assertEqual(len(detection_result.detected_objects), 2)
        self.assertEqual(str(detection_result.image_id), 'channel1--1541860141--jpg')
