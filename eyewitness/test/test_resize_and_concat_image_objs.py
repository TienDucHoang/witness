import unittest
from pathlib import Path

from eyewitness.image_utils import ImageHandler
from eyewitness.image_utils import resize_and_stack_image_objs


class ResizeAndConcatTest(unittest.TestCase):
    def test_image_swap(self):
        image_path = str(Path('eyewitness', 'test', 'pics', 'pikachu.png'))
        normal_pikachu = ImageHandler.read_image_file(image_path).convert('RGB')
        img_objs = [normal_pikachu, normal_pikachu, normal_pikachu]
        resize_shape = (64, 64)
        batch_images_array = resize_and_stack_image_objs(resize_shape, img_objs)
        self.assertEqual(batch_images_array.shape, (3, 64, 64, 3))
