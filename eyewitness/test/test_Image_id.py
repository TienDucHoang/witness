import unittest

import arrow
from eyewitness.image_id import ImageId


class ImageIdTest(unittest.TestCase):
    def test_image_id_cmp(self):
        img_timestamp = arrow.get('2013-05-05 12:30:45', 'YYYY-MM-DD HH:mm:ss').timestamp
        image1 = ImageId('channel_1', img_timestamp)
        image2 = ImageId('channel_1', img_timestamp)
        image3 = ImageId('channel_2', img_timestamp)
        image4 = ImageId('channel_1', img_timestamp, file_format='png')
        self.assertEqual(image1, image2)
        self.assertNotEqual(image3, image2)
        self.assertNotEqual(image1, image4)

    def test_image_id_as_key(self):
        img_timestamp = arrow.get('2013-05-05 12:30:45', 'YYYY-MM-DD HH:mm:ss').timestamp
        image1 = ImageId('channel_1', img_timestamp)
        image2 = ImageId('channel_2', img_timestamp)
        x = {image1: 5566, image2: 1126}
        image3 = ImageId('channel_2', img_timestamp)
        self.assertEqual(x[image1], 5566)
        self.assertEqual(x[image2], 1126)
        self.assertEqual(x[image3], 1126)
