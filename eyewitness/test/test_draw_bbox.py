import unittest
from pathlib import Path

from eyewitness.image_utils import ImageHandler
from eyewitness.config import BoundedBoxObject


class DrawBboxTest(unittest.TestCase):
    def test_image_draw(self):
        image_path = str(Path('eyewitness', 'test', 'pics', 'pikachu.png'))
        x = ImageHandler.read_image_file(image_path)
        obj = BoundedBoxObject(250, 100, 800, 900, 'pikachu', 0.5, '')
        ImageHandler.draw_bbox(x, [obj])
