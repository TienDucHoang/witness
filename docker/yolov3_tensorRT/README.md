# Yolov3 implementation with TensorRT

https://github.com/penolove/yolov3-tensorrt branch: eyeWitnessWrapper

this is a fork from https://github.com/xuwanqi/yolov3-tensorrt, and wrapper with eyewitness 

actually the origin repo from xuwanqi is just the same as example from JetPack example 
https://docs.nvidia.com/deeplearning/sdk/tensorrt-sample-support-guide/index.html

which transform the yolov3 [darknet implementation](https://pjreddie.com/darknet/yolo/) -> onnx -> tensorRT


## prepare yolov3 weights
```bash
mkdir weights
mkdir detected_image
mkdir raw_image
mkdir db_folder  # used for end2end example
wget -O weights/yolov3.weights https://pjreddie.com/media/files/yolov3.weights
```

## Transform yolov3 model into TensorRT engine

start and access container first
```bash
# start container
docker run \
    --name yolov3_trt \
    --gpus all \
    -v $PWD/weights:/workspace/yolov3-tensorrt/weights \
    -v $PWD/detected_image:/workspace/yolov3-tensorrt/detected_image \
    -d  penolove/tensorrt_yolo_v3:gpu \
    tail -f /dev/null;

# access into container
docker exec -ti yolov3_trt /bin/bash

# convert model into tensorRT engine
cd yolov3-tensorrt/;
python yolov3_to_onnx.py  # convert darknet into onnx
python onnx_to_tensorrt.py  # build tensorRT engine and store it to yolov3.engine
```

## naive_example: using eyewitness to wrapper the classifier
```bash
# make sure the yolov3.engine were generated
python naive_detector.py --engine_file yolov3.engine
```

## detector flask
```bash
# make sure the yolov3.engine were generated
python detector_with_flask.py  \
    --engine_file yolov3.engine \
    --db_path db_folder/example.sqlite \
    --drawn_image_dir detected_image \
    --detector_host 0.0.0.0
```

```bash
# post a 56 image 
wget https://upload.wikimedia.org/wikipedia/commons/2/25/5566_and_Daily_Air_B-55507_20050820.jpg -O raw_image/5566.jpg

curl -X POST \
  http://localhost:5566/detect_post_path \
  -H 'content-type: application/json' \
  -H 'image_id: 5566--1541860142--jpg' \
  -H 'raw_image_path: /workspace/yolov3-tensorrt/raw_image/5566.jpg'
```

## celery detector worker example 
0. start celery work
```
cd gpu/celery;
# start a celery woker and rabbitmq server.
# this will build the engine, so it will take times to start celery worker
# you can edit the compose file to figure out the best concurrency setting, worker amount
# for your device
docker-compose up -d
```

1. access the yolov3_trt container and sent a celery task
```
docker-compose exec yolov3_trt /bin/bash
```

2. sent image task
```python
import arrow
from celery_tasks import detect_image
params = {
  'channel': '5566',
  'timestamp': arrow.now().timestamp,
  'is_store_detected_image': True,
}
detect_image.si(params).apply_async()
```


## Build image from Dockerfile:
- gpu
```
docker build --no-cache -t penolove/tensorrt_yolo_v3:gpu gpu/
```



