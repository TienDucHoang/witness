# Trident

https://github.com/penolove/simpledet.git branch: eyeWitnessWrapper

this is a fork from https://github.com/TuSimple/simpledet, and wrapper with eyewitness 

## pre-requirement
- install docker / nvidia-docker(for gpu)

### 1. prepare the data required
```
mkdir weights
# download a annotation example which used for simpledet
wget https://dl.dropboxusercontent.com/s/o43o90bna78omob/instances_minival2014.json.zip
unzip instances_minival2014.json.zip -d weights

# download a pretrained Trident model with having 40.6 mAP on coco dataset
wget https://simpledet-model.oss-cn-beijing.aliyuncs.com/tridentnet_r101v2c4_c5_1x.zip
unzip tridentnet_r101v2c4_c5_1x.zip -d weights
```


## Quick Start
### Run from built docker image:
```
nvidia-docker run --name simpledet \
                  -v $PWD/weights/instances_minival2014.json:/simpledet/data/coco/annotations/instances_minival2014.json \
                  -v $PWD/weights/tridentnet_r101v2c4_c5_1x:/simpledet/experiments/tridentnet_r101v2c4_c5_1x \
                  -d penolove/simpledet:gpu \
                  tail -f /dev/null;
```


```
# access the container
nvidia-docker exec -ti simpledet /usr/bin/zsh

cd simpledet
make;
python3 naive_detector.py --config config/tridentnet_r101v2c4_c5_1x.py
```

## Build your own image

- gpu
```
nvidia-docker build --no-cache -t penolove/simpledet:gpu gpu/
```

