
## Quick Start

detector repo: https://github.com/penolove/cv2-object-detector

there are three wrapper examples for opencv 
1. Hog Person detector
2. Haar Cascade Person detector
3. MobileNet detector(weight already in cv2-object-detector repo)


### Run from built docker image:
#### cpu (~ 1.29 GB)
```
# run container
docker run --name cv2_detector \
           -d penolove/cv2_detector:cpu \
           tail -f /dev/null;

# access container 
docker exec -ti cv2_detector /bin/bash;
cd cv2-object-detector/
# run a cv2 with caffe pre-trained MobileNet example
python3 naive_detector.py 
```

#### raspberry-pi (~4.9GB make sure your disk is large enough)
```
# run container
docker run --name cv2_detector \
           -d penolove/cv2_detector:raspberry-pi \
           tail -f /dev/null;

# access container 
docker exec -ti cv2_detector /bin/bash;
cd cv2-object-detector/
# run a cv2 with caffe pre-trained MobileNet example
python3 naive_detector.py 
```



## Build image from Dockerfile:
### cpu
```
docker build --no-cache -t penolove/cv2_detector:cpu cpu/
```

### raspberry-pi
```
docker build --no-cache -t penolove/cv2_detector:raspberry-pi raspberry-pi/
```
