# MTCNN for face detection

https://github.com/penolove/mtcnn branch: eyeWitnessWrapper

this is a fork from https://github.com/DuinoDu/mtcnn, and a wrapper example with eyewitness 


## Run a example
### CPU
```
# run container
docker run --name mtcnn \
           -d penolove/mtcnn:cpu \
           tail -f /dev/null;

# access container 
docker exec -ti mtcnn /bin/bash;
cd /workspace/mtcnn
# run a mtcnn detector example, the detected image will be stored in 
# mtcnn/detected_image/drawn_image.jpg
python naive_detector.py --gpu_id -1
```

### GPU
```
# run container
nvidia-docker run --name mtcnn \
           -d penolove/mtcnn:gpu \
           tail -f /dev/null;

# access container 
docker exec -ti mtcnn /bin/bash;
cd /workspace/mtcnn
# run a mtcnn detector example, the detected image will be stored in 
# mtcnn/detected_image/drawn_image.jpg
python naive_detector.py --gpu_id 0
```


## Build image from Dockerfile:
### cpu
```
docker build --no-cache -t penolove/mtcnn:cpu cpu/
```

### gpu
```
nvidia-docker build --no-cache -t penolove/mtcnn:gpu gpu/
```

